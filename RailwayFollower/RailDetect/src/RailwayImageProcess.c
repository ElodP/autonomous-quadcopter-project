
#include "RailwayImageProcess.h"


#define PI  3.14159265

//****** Soble  3X3 matrices ******
int dx[3][3] = {{1,0,-1},{2,0,-2},{1,0,-1}};
int dy[3][3] = {{1,2,1},{0,0,0},{-1,-2,-1}};
//*********************************

// Detecting if a point is inside of a triangle
// like VP bottom corners of imgage.
bool pointInTriangel(Point A, Point B, Point C, Point P )
{
	float v0x=C.x-A.x, v0y=C.y-A.y, v1x=B.x-A.x, v1y=B.y-A.y, v2x=P.x-A.x, v2y=P.y-A.y;
	float dot00 =v0x*v0x+v0y*v0y;
	float dot01 =v0x*v1x+v0y*v1y;
	float dot02 =v0x*v2x+v0y*v2y;
	float dot11 =v1x*v1x+v1y*v1y;
	float dot12 =v1x*v2x+v1y*v2y;
	float invDenom = 1/(dot00 * dot11 - dot01 * dot01);
	float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	float v = (dot00 * dot12 - dot01 * dot02) * invDenom;
	if (u>=0 && v>=0 && u+v<=1)
		return true;
	else
		return false;
}

// Searching lines between rail track
// actually lines between the VP and the bottom left and right corners
// !!!!!!!!!!!!!!!!!!!maybe need to be adjusted!!!!!!!!!!!!!!!!!!!!!!!!!!!
float serachYaw(vector<Vec4i>lines, Point VP, int maxX, int maxY, IplImage * result)
{
	Vec4i l;
	float theSum=0.0;
	int nrLines=0;
	for (size_t i = 0; i < lines.size(); i++)
	{	
		Point A =Point(0, maxY), B= Point(maxX, maxY);
		l = lines[i];
		if (pointInTriangel(A,B,VP,Point(l[0],l[1])) && pointInTriangel(A,B,VP,Point(l[2],l[3])) ) 
		{	
			theSum=theSum+(atan2(l[1]-l[3],l[2]-l[0])*180/PI +PI);
			nrLines=nrLines+1;
			cvLine( result, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,0),1);				
		}
	}
	if (nrLines==0) return 0.0;
	return (theSum/nrLines);
}


// Searching for VP between the filtered lines
Point searchVP(vector<Vec4i> lines, IplImage * result)
{
int sizeX=result->width/33, sizeY=result->height/22;
Point vanishP1 = Point(-1,-1);
if (lines.size()>1 && sizeX>0 && sizeY>0)
	{
	int imgGrid[22][33];    // grid of crossing points              
        for (int i=0;i<22;i++) // initialization
	        {
                for (int j=0;j<33;j++) imgGrid[i][j]=0;
                }
	Point * cP;
	// Filling up the grid with crossing points
	if (lines.size()==2)
		{
		cP=crossPoint(lines[0],lines[1]);
		if (cP->x>0.0 && cP->y>0.0 && cP->x<sizeX*22.0 && cP->y<sizeY*33.0)
			{
			imgGrid[int(cP->x/sizeX)][int(cP->y/sizeY)]++;
			}			
	
		}else{
		for (int i=0;i<lines.size()-1;i++)
			{	
			for (int j=i+1; j<lines.size(); j++)
				{
				cP=crossPoint(lines[i],lines[j]);
		                if (cP->x>0.0 && cP->y>0.0 && cP->x<sizeX*22.0 && cP->y<sizeY*33.0)
                		        {
		                        cvCircle(result,cvPoint(int(cP->x),int(cP->y)),3,Scalar(220,0,0),-1,8,0);
					imgGrid[int(cP->x/sizeX)][int(cP->y/sizeY)]++;
                		        }
				}
			}
		}
	delete cP;
	// Searching for the most dens aria of intersections
	int countP=0,maxi=0, maxj=0;
        for (int i=0;i<22;i++)
	        {
                for (int j=0;j<33;j++)
         	       {
                       if (countP<imgGrid[i][j])
                       		{
                                countP=imgGrid[i][j];
                                maxi=i;
                                maxj=j;
                                }
                       }
               	}
	if (countP>0) 
        	{
                vanishP1 = cvPoint(maxi*sizeX,maxj*sizeY);
		Point vanishP2 = cvPoint((maxi+1)*sizeX,(maxj+1)*sizeY);
                CvScalar red= CV_RGB(220,0,0);                      
                cvRectangle(result,vanishP1,vanishP2,red,2,8,0); 
		vanishP1 = cvPoint((maxi+0.5)*sizeX,(maxj+0.5)*sizeY);               

                }else{
		vanishP1 = Point(-1,-1);
		printf("NO vanishing point on the img \n");
		} 		
	}else{
	vanishP1 = Point(-1,-1);
	printf("NOT enough lines \n");
	}
return vanishP1;
}


//*********** line Processing *********************************
// Here we filter out, the unnecessary lines for the railway detection Get the VP and the Yaw orientation
Point2f LineProcessing(vector<Vec4i> lines, IplImage * result)
{
	vector<Vec4i> deletedLines;	
	// Filtering of the lines regarding the angle, to find tracks
	size_t i = 0;
	while (i < lines.size())
		{
		Vec4i l=lines[i];		
		float the=atan2(l[1]-l[3],l[2]-l[0])*180/PI;
// For simulation taken out from line filtering, maybe necessary in turns
// || (abs(the)<110&&abs(the)>75) ||(abs(the)<285&&abs(the)>245)
		if ( abs(the)<10 || (abs(the)<188 && abs(the)>172)|| (abs(the)<110&&abs(the)>75) ||(abs(the)<285&&abs(the)>245))
			{
			lines.erase(lines.begin()+i);
			deletedLines.push_back (l);
			}else{			
			cvLine( result, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,0,0), 1);			
			i++;	
			}
		}
		// Vanishing Point detection if not found return is (-1,-1)
		Point VP = searchVP(lines, result);
		float angle = 360;
		if (VP.x!= -1 && VP.y!=-1)
		{
			// The lines between the rail track showing the orientation of the track
			angle=serachYaw(deletedLines, VP, result->width, result->height,result);			
		}
	// The returned point has the x coordinate of VP and the yaw angle
	return Point2f(VP.x,angle);
}


//*******************Crossing points of 2 lines ********
Point * crossPoint(cv::Vec4i l1, cv::Vec4i l2)
{
	Point* cP;
	cP= new Point;
	Point o1 (l1[0], l1[1]) ; // start point line1
        Point p1 (l1[2], l1[3]) ; // end point line 1
        Point o2 (l2[0], l2[1]) ; // start point line2
        Point p2 (l2[2], l2[3]) ; // end point line2
        Point x = o2 - o1;
        Point d1 = p1 - o1;
        Point d2 = p2 - o2;
        float cross = d1.x*d2.y - d1.y*d2.x;
        if (abs(cross) > 1e-8)
        	{
            	double t1 = (x.x * d2.y - x.y * d2.x)/cross;
                * cP= o1 + d1 * t1;
		}else{
		Point cPt(-1,-1);
		* cP=cPt; 
		}
	return cP;
}

//// ****************HoughLinesP****************
vector<Vec4i> PHTlines(IplImage * img, int tresh,int min, int max, const char* win_name, bool drawWindow ){
	if (tresh == 0 || min == 0 || max == 0)
	{	
		tresh =200;
		min = 45;
		max = 2;
	}
	IplImage * cdst =cvCreateImage( cvSize(img->width,img->height), 8,3);
	cvCvtColor(img,cdst,CV_GRAY2RGB);
	vector<Vec4i> lines;
	HoughLinesP(Mat(img), lines, 1, CV_PI/180, tresh, min, max );	
	if (drawWindow){
		for( size_t i = 0; i < lines.size(); i++ )
		{
			Vec4i l = lines[i];
			cvLine( cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
			for (size_t j= i+1; j<lines.size(); j++)
			{
				Vec4i lnext = lines[j];	
				Point * tmp =crossPoint(l,lnext);
				cvCircle(cdst,cvPoint(tmp->x, tmp->y),1,CV_RGB(0,255,0),-1);
			}
		}
		cvShowImage( win_name, cdst);
		cvReleaseImage( &cdst );
	}
	return lines;
}

//************************Dilation and Erodeing*********************
IplImage * ErodeDilate(IplImage *img,int e, int d)
{
	cvErode(img,img,NULL,e);
	cvDilate(img,img,NULL,d);
	return img;
}

// **************************Laplace Filter**************************
//Filter for Laplace
int **getFilterInt(int size, int v)
{
	int **filter = 0;
	filter = new int *[size] ;
	for( int i = 0 ; i < size ; i++ )
	filter[i] = new int[size];
	int value=v;
	switch (value)
	{
	case 2: 
		filter[0][0] = 1; filter[0][1] = 1; filter[0][2] = 1;
		filter[1][0] = 1; filter[1][1] = -8; filter[1][2] = 1;
		filter[2][0] = 1; filter[2][1] = 1; filter[2][2] = 1;
		break;
	case 1:
		filter[0][0] = 0; filter[0][1] = 1; filter[0][2] = 0;
		filter[1][0] = 1; filter[1][1] = 4; filter[1][2] = 1;
		filter[2][0] = 0; filter[2][1] = 1; filter[2][2] = 0;
		break;
	case 3:
		filter[0][0] = 0; filter[0][1] = -1; filter[0][2] = 0;
		filter[1][0] = -1; filter[1][1] = 4; filter[1][2] = -1;
		filter[2][0] = 0; filter[2][1] = -1; filter[2][2] = 0;
		break;

	case 4:
		filter[0][0] = -1;filter[0][1] = -1; filter[0][2] = -1;
		filter[1][0] = -1; filter[1][1] = 8; filter[1][2] = -1;
		filter[2][0] = -1; filter[2][1] = -1; filter[2][2] = -1;
		break;
	}
	return filter;
}
// Laplace Filter
IplImage * laplaceFilter(IplImage *img, int filterSize,  int FilterType, int v)
{	
	int **filter = getFilterInt(filterSize,FilterType);	
	int c=v;
	IplImage *imgR = cvCreateImage(cvSize(img->width, img->height), 8, 1);
	int value;
	for(int i = 0;i<img->height;i++)
	{
		for(int j = 0;j<img->width;j++)
		{
			value = 0;
			int control = 0;
			for(int k = -1*(int)filterSize/2 ; k<= (int)filterSize/2;k++)
			{
				for(int n = -1*(int)filterSize/2 ; n<= (int)filterSize/2;n++)
				{
					if(j+n<0 || i+k >= img->height || j+n>=img->width || i+k<0){control++;continue;}
					value += filter[(int)filterSize/2+k][(int)filterSize/2+n] * (int)cvGet2D( img, i+k, j+n ).val[0];
				}
			}
			CvScalar s;
			s= cvGet2D(img, i, j );
			value =  c*(int) value;
			if(value<0) 
			{
				value = 0;
			}
			CvScalar r;
			r = cvGet2D(imgR,i,j);
			r.val[0] = value;
			cvSet2D(imgR, i, j, r);
		}
	}
	return imgR;
}


//******** Sobel ******** faster versions  *****************
// Sobel operator implementation using inderect access
void Sobel_ind_fast( IplImage* img, IplImage* dst)
{
	CvScalar s;
	for (int i=1; i < img->height-2; i++)
	for (int j=1; j < img->width-2; j++)
	{
		// Apply kernel in X and Y directions
		int sum_x=0;
		int sum_y=0;
		for(int m=-1; m<=1; m++)
		for(int n=-1; n<=1; n++)
		{
			s=cvGet2D(img,i+m,j+n); // get the (i,j) pixel value
			sum_x+=(int)s.val[0]*dx[m+1][n+1];
			sum_y+=(int)s.val[0]*dy[m+1][n+1];
		}
		int sum=abs(sum_x)+abs(sum_y);
		s.val[0]=(sum>255)?255:sum;
		cvSet2D(dst,i,j,s); // set the (i,j) pixel value
	}
}

// Sobel operator implementation using direct access using a pointer
void Sobel_dir_fast( IplImage* img, IplImage* dst)
{
	int step = img->widthStep/sizeof(uchar);
	uchar* data = (uchar *)img->imageData;
	uchar* data_dst = (uchar *)dst->imageData;	
	int s;
	for (int i=1; i < img->height-2; i++)
	for (int j=1; j < img->width-2; j++)
	{
		// Apply kernel in X ans Y direction
		int sum_x=0;
		int sum_y=0;
		for(int m=-1; m<=1; m++)
		for(int n=-1; n<=1; n++)
		{
			s=data[(i+m)*step+j+n]; // get the (i,j) pixel value
			sum_x+=s*dx[m+1][n+1];
			sum_y+=s*dy[m+1][n+1];
		}
		int sum=abs(sum_x)+abs(sum_y);
		data_dst[i*step+j]=(sum>255)?255:sum; // set the (i,j) pixel value
	}
}
// End of Sobel faster versions 

//********* Sobel operator implementation using indirect access
void Sobel_ind( IplImage* img, IplImage* dst)
{
	CvScalar s;
	for (int i=1; i < img->height-2; i++)
	for (int j=1; j < img->width-2; j++)
	{
		// Apply kernel in X direction
		int sum_x=0;
		for(int m=-1; m<=1; m++)
		for(int n=-1; n<=1; n++)
		{
			s=cvGet2D(img,i+m,j+n); // get the (i,j) pixel value
			sum_x+=(int)s.val[0]*dx[m+1][n+1];
		}
		// Apply kernel in Y direction
		int sum_y=0;
		for(int m=-1; m<=1; m++)
		for(int n=-1; n<=1; n++)
		{
			s=cvGet2D(img,i+m,j+n); // get the (i,j) pixel value
			sum_y+=(int)s.val[0]*dy[m+1][n+1];
		}
		int sum=abs(sum_x)+abs(sum_y);
		if (sum>255)
		sum=255;
		s.val[0]=sum;
		cvSet2D(dst,i,j,s); // set the (i,j) pixel value
	}
}

// Sobel operator implementation using direct access using a pointer
void Sobel_dir( IplImage* img, IplImage* dst)
{

	int step = img->widthStep/sizeof(uchar);
	uchar* data = (uchar *)img->imageData;
	uchar* data_dst = (uchar *)dst->imageData;
	
	int s;
	for (int i=1; i < img->height-2; i++)
	for (int j=1; j < img->width-2; j++)
	{
		// Apply kernel in X direction
		int sum_x=0;
		for(int m=-1; m<=1; m++)
		for(int n=-1; n<=1; n++)
		{
			s=data[(i+m)*step+j+n]; // get the (i,j) pixel value
			sum_x+=s*dx[m+1][n+1];
		}
		// Apply kernel in Y direction
		int sum_y=0;
		for(int m=-1; m<=1; m++)
		for(int n=-1; n<=1; n++)
		{
			s=data[(i+m)*step+j+n]; // get the (i,j) pixel value
			sum_y+=s*dy[m+1][n+1];
		}
		int sum=abs(sum_x)+abs(sum_y);
		if (sum>255)
		sum=255;
		data_dst[i*step+j]=sum; // set the (i,j) pixel value
	}
}
