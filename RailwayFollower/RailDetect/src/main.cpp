// Personal Library dealing with image processing VP detection
#include "RailwayImageProcess.h"

#include <iostream>
#include <fstream>
#include <image_transport/image_transport.h>
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/Image.h"
#include <sensor_msgs/image_encodings.h>
// Img processing 
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
// Kalman filter
#include <opencv/cv.h>
// Control message including
#include <time.h>
#include <RailDetect/RailFlyCommand.h>
#include <std_msgs/Empty.h>
#include <std_srvs/Empty.h>

// Time checker
double secs;

#define PI  3.1415926
#define tresholdH 65

namespace enc = sensor_msgs::image_encodings;
using namespace cv;
using namespace std;

RailDetect::RailFlyCommand controlMsg;

// Declare a string with the name of the window that we will create using OpenCV where processed images will be displayed.
static const char WINDOW[] = "Image Processed corridor";
//sensor_msgs::cv_bridge bridge_;

// Use method of ImageTransport to create image publisher
image_transport::Publisher pub;
// Publisher for fly control 
ros::Publisher pubFly;

// -------------Kalman estimation--------------------

KalmanFilter KF(2,1,0);

Mat_<float> state(2, 1); /* (x, Vx, Yaw) */

Mat processNoise(2, 1, CV_32F);
Mat_<float> measurmentVP(1,1);
vector<Point> kalmanVec;
int nrNotDetect=0;
Point observation, estimation;
ros::Time frameAt;
double deltaT=0.0, lastTimeStamp=0.0;
int sigmaV=100, sigmaU=70;
float yaw=0.0;


void whereToFly(Point2f o, Point e, bool stop)
{
	RailDetect::RailFlyCommand controlMsg;
	controlMsg.rotation = "no"; // left right no
	controlMsg.direction = "no"; // left right no
	controlMsg.ahead = "no"; // front back no land 
	controlMsg.speed = 0.0; // float in (-1,1)!! 
	controlMsg.Dtime=deltaT;
	if (stop)
	{
		if (o.x < 320+10) controlMsg.direction = "right";
		else if (o.x > 320-10) controlMsg.direction = "left";
		controlMsg.ahead = "front";
		controlMsg.speed = 0.09;
	}else{
		controlMsg.ahead = "land";
	}
	// VP_x=p.x;
	controlMsg.observationX=o.x;
	// Yaw =p.y;
	controlMsg.observationY=o.y;
	controlMsg.estimationX=e.x;
	controlMsg.estimationY=e.y;
	controlMsg.frameTimeStamp=frameAt;
	controlMsg.yawObservatino=o.y;
	pubFly.publish(controlMsg);
}


void initKF()
{	measurmentVP.setTo(Scalar(0.0));
	// Init state
	KF.statePre.at<float>(0) = 320;
	KF.statePre.at<float>(1) = 0;
	// Process noise covariance matrix (Q) 
	KF.processNoiseCov.at<float>(0) = sigmaU*pow(deltaT,4)/3;
	KF.processNoiseCov.at<float>(1) = sigmaU*pow(deltaT,3)/2;
	KF.processNoiseCov.at<float>(2) = sigmaU*pow(deltaT,3)/2;
	KF.processNoiseCov.at<float>(3) = sigmaU*deltaT;
	/* Setup parameters for a more complex model, see publication for more information */
	/*
	KF.processNoiseCov.at<float>(4) = pow(sigmaU,2)*pow(deltaT,3)/3;
	KF.processNoiseCov.at<float>(5) = pow(sigmaU,2)*pow(deltaT,2)/2;
	KF.processNoiseCov.at<float>(6) = pow(sigmaU,2)*pow(deltaT,3)/6;
	KF.processNoiseCov.at<float>(7) = pow(sigmaU,2)*pow(deltaT,2)/2;
	KF.processNoiseCov.at<float>(8) = pow(sigmaU,2)*deltaT; 
	*/
	
// Measurement noise covariance matrix (R) 
	KF.measurementNoiseCov = sigmaV;
// Measurement matrix (H) 
	setIdentity(KF.measurementMatrix);	
	KF.transitionMatrix = *(Mat_<float>(2, 2) << 1,deltaT, 0,1);//*(cos(yaw)-sin(yaw))
	setIdentity(KF.measurementMatrix);	
	setIdentity(KF.errorCovPost, Scalar::all(50));
	kalmanVec.clear();
}

void reClacKFparameters(Point at)
{
	KF.processNoiseCov.at<float>(0) = sigmaU*pow(deltaT,4)/3;
	KF.processNoiseCov.at<float>(1) = sigmaU*pow(deltaT,3)/2;
	KF.processNoiseCov.at<float>(2) = sigmaU*pow(deltaT,3)/2;
	KF.processNoiseCov.at<float>(3) = sigmaU*deltaT;
	KF.transitionMatrix = *(Mat_<float>(2, 2) << 1,deltaT, 0,1);
}


Point estimationVP(Point at){
	// Estimation
	Mat prediction = KF.predict();
	Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
	measurmentVP(0) = at.x;
	measurmentVP(2) = 0;
	// Update
	Mat estimated = KF.correct(measurmentVP);
	Point statePt(estimated.at<float>(0),estimated.at<float>(1));     
	kalmanVec.push_back(statePt);
	return statePt ;
}


// Search for the corridor end/ vanishing point 
void railWayProces(IplImage *result)
{
	IplImage * imageOriginal = cvCreateImage(cvSize(result->width,result->height),8,3);
	cvCopy(result, imageOriginal);
	cvSmooth( imageOriginal, imageOriginal, CV_GAUSSIAN, 11, 11 );
	// Grayscaling the image.
	IplImage * gray =cvCreateImage(cvSize(imageOriginal->width,imageOriginal->height),8,1);
	IplImage * grayS =cvCreateImage(cvSize(imageOriginal->width,imageOriginal->height),8,1);
	cvCvtColor(imageOriginal,grayS,CV_RGB2GRAY);	
	gray = laplaceFilter(imageOriginal,3,3,20);
	// Selecting the darker grey colors on the filtered image
	cvThreshold(gray, grayS, 70, 255,THRESH_TOZERO_INV );
	// Amplifying the whit intensive shades
	cvSub(gray,grayS,grayS);
	cvAdd(gray,grayS,grayS);		
	vector<Vec4i> lines=PHTlines(grayS, 150, 40, 4, "PHT", false);
	Point2f  VP_Yaw = LineProcessing(lines, result); 	
	if (VP_Yaw.x >-1) 	{
		// Estimate position
		reClacKFparameters(VP_Yaw);
		estimation=estimationVP(VP_Yaw);	
	}
	else
	{		
		reClacKFparameters(VP_Yaw);
		Mat prediction = KF.predict();
		estimation = Point(prediction.at<float>(0),prediction.at<float>(2));			
	}
	whereToFly( VP_Yaw, estimation, true);
}


//------------------- Receiving video frames -------------------------------------------------------
// Function is called every time a new image is published
void imageCallback(const sensor_msgs::ImageConstPtr& msg_ptr)
{

  
  cv_bridge::CvImagePtr cv_ptr;
  IplImage *cv_image;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg_ptr, enc::BGR8);

	cv::Mat image1 = cv_ptr->image;
	
	cv_image = cvCreateImage(cvSize(image1.cols,image1.rows),8,3);
	IplImage ipltemp=image1;
	cvCopy(&ipltemp,cv_image);		

  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("error");
  
  	railWayProces(cv_image);
	cvCircle(cv_image,Point(estimation.x,50),4,Scalar(0,200,0),-1,8,0);
	cvLine(cv_image, Point(estimation.x,50), Point(estimation.x,360),Scalar(0,200,0),1,8,0);
        cvLine(cv_image, Point(320,0), Point(320,360),Scalar(0,0,0),1,8,0);
	if (lastTimeStamp == 0.0) 
	{
		lastTimeStamp=msg_ptr->header.stamp.toSec();
	}
	deltaT=(msg_ptr->header.stamp.toSec()-lastTimeStamp)*10;
	lastTimeStamp=msg_ptr->header.stamp.toSec();	
  }
  cvWaitKey(3);
  try
  {
	cv_ptr->image=Mat(cv_image);
	pub.publish(cv_ptr->toImageMsg());
    //pub.publish(bridge.cvToImgMsg(cv_image, "bgr8"));
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("error");
  }
	cvReleaseImage(&cv_image);
}

bool gameOver = false;

void watchCallback(const std_msgs::Empty& msgEmpty)
{
	gameOver = true;
}

//------------- MAIN ----------------------------------------------------------
int main(int argc, char **argv)
{
	initKF();	
	ros::init(argc, argv, "railway_processor");
	ros::NodeHandle nh;
	// Create an ImageTransport instance, initializing it with our NodeHandle.
	image_transport::ImageTransport it(nh);
	// OpenCV HighGUI call to create a display window on start-up.
	cv::namedWindow(WINDOW, CV_WINDOW_NORMAL);	
	image_transport::Subscriber sub = it.subscribe("/ardrone/image_raw", 1, imageCallback);
	// Flight direction massage publisher
	pubFly = nh.advertise<RailDetect::RailFlyCommand>("/railway/fly_command", 1);
	// OpenCV HighGUI call to destroy a display window on shut-down.
	cv::destroyWindow(WINDOW);	
	pub = it.advertise("/railway/image_processed", 1);		
	ros::NodeHandle node;
	ros::Subscriber sub2 = node.subscribe("/ardrone/land", 1, watchCallback);		
	ros::spinOnce();	
	ros::Rate rate(100);
	while(ros::ok() && !gameOver)
	{
			rate.sleep();
			ros::spinOnce();
	}	
}

